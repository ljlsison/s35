const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

dotenv.config()

const app = express()
const port = 3002

mongoose.connect(`mongodb+srv://cinnamontoast:${process.env.MONGODB_PASSWORD}@cluster0.4pgcafz.mongodb.net/?retryWrites=true&w=majority`,{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', () => console.error(`Connection Error`))
db.on('open', () => console.log(`Connected to MongoDB`))

app.use(express.json())
app.use(express.urlencoded({extended: true}))

const taskSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model('User', taskSchema)

app.post('/signup', (request, response) => {
	User.findOne({username: request.body.username}, (error,result) => {
		if(result !== null && result.username == request.body.username){
			return response.send(`User ${result.username} already exists!`)
		}

		let newUser = new User({
			username: request.body.username,
			password: request.body.password
		})

		newUser.save((error, savedUser) => {
			if(error){
				return console.error(error)
			}else{
				return response.status(200).send('User has registered successfully')
			}
		})
	})
})

app.listen(port, () => console.log(`Server is running at localHost:${port}`))